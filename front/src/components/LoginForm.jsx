import React from 'react';
import axios from 'axios';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    /*
    const api = axios.create({
        baseURL: 'https://some-domain.com/api/',
        timeout: 1000,
        headers: {'X-Custom-Header': 'foobar'}
      });
      
    api.post()
      .then()
      .catch();
*/
    localStorage.setItem('currentUser', this.state.username);
    localStorage.setItem('token', 'tokenqedfklvjqdklfvjqdlkfjv');
  };

  render() {
    return (
      <form method="POST" onSubmit={this.handleSubmit}>
        <input type="text" name="username" onChange={this.handleChange} />
        <input type="password" name="password" onChange={this.handleChange} />
        <button>Send data !</button>
      </form>
    );
  }
}
export default LoginForm;

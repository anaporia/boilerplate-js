import React from 'react';

class UserHome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const currentUser = localStorage.getItem('currentUser');
    return <p>UserHome for {currentUser}!</p>;
  }
}
export default UserHome;
